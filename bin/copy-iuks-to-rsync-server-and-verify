#!/usr/bin/python3

import argparse
import logging
import subprocess
import sys

from typing import List
from pathlib import Path

JENKINS_IUKS_BASE_URL = "https://nightly.tails.boum.org/build_IUKs/builds"
RSYNC_SERVER_HOSTNAME = "rsync.lizard"
LOG_FORMAT = "%(asctime)-15s %(levelname)s %(message)s"
log = logging.getLogger()


def main():
    parser = argparse.ArgumentParser(
        description="Copy IUKs from Jenkins to our rsync server \
        and verify that they match those built locally"
    )
    parser.add_argument("--hashes-file", type=str, action="store", required=True)
    parser.add_argument("--jenkins-build-id", type=int, action="store", required=True)
    parser.add_argument("-q", "--quiet", action="store_true",
                        help="quiet output")
    parser.add_argument("--debug", action="store_true", help="debug output")
    parser.add_argument("--skip-sending-hashes-file", action="store_true",
                        help="Assume the hashes file was uploaded already")
    parser.add_argument("--skip-downloading-iuks", action="store_true",
                        help="Assume the IUKs were already downloaded")
    args = parser.parse_args()

    if args.debug:
        logging.basicConfig(level=logging.DEBUG, format=LOG_FORMAT)
    elif args.quiet:
        logging.basicConfig(level=logging.WARN, format=LOG_FORMAT)
    else:
        logging.basicConfig(level=logging.INFO, format=LOG_FORMAT)

    if not Path(args.hashes_file).exists():
        log.error("%s does not exist" % (args.hashes_file))
        sys.exit(1)

    if not args.skip_sending_hashes_file:
        send_hashes_file(
            hashes_file=args.hashes_file,
            desthost=RSYNC_SERVER_HOSTNAME,
        )

    if not args.skip_downloading_iuks:
        download_iuks_from_jenkins(
            hashes_file=args.hashes_file,
            desthost=RSYNC_SERVER_HOSTNAME,
            jenkins_iuks_base_url=JENKINS_IUKS_BASE_URL,
            jenkins_build_id=args.jenkins_build_id,
        )

    verify_iuks(
        desthost=RSYNC_SERVER_HOSTNAME,
        hashes_file=Path(args.hashes_file).name,
    )


def send_hashes_file(
        hashes_file: str,
        desthost: str) -> None:
    log.info("Sending %(f)s to %(h)s…" % {
        "f": hashes_file,
        "h": desthost,
    })
    subprocess.run(
        ["scp", hashes_file, "%s:" % (desthost)],
        check=True
    )


def iuks_listed_in(hashes_file: str) -> List[str]:
    with Path(hashes_file).open() as f:
        lines = f.readlines()
    return [l.split('  ')[-1].rstrip() for l in lines]


def download_iuks_from_jenkins(
        hashes_file: str,
        desthost: str,
        jenkins_iuks_base_url: str,
        jenkins_build_id: int) -> None:
    log.info("Downloading IUKs from Jenkins to %s…" % (desthost))
    iuks = iuks_listed_in(hashes_file)
    log.debug("IUKS: %s" % ', '.join(iuks))
    for iuk in iuks:
        log.debug("Downloading %s" % (iuk))
        url = "%s/%s/archive/%s" % (
            jenkins_iuks_base_url,
            jenkins_build_id,
            iuk
        )
        subprocess.run(
            ["ssh", desthost, "wget", "--quiet", "--no-clobber",
             "-O", iuk, url],
            check=True
        )


def verify_iuks(desthost: str, hashes_file: str) -> None:
    log.info("Verifying that IUKs built on Jenkins match those you've built…")
    try:
        subprocess.run(
            ["ssh", desthost, "sha256sum", "--check", "--strict",
             Path(hashes_file).name],
            check=True
        )
    except subprocess.CalledProcessError:
        print("\nERROR: IUKs built on Jenkins don't match yours\n",
              file=sys.stderr)


if __name__ == "__main__":
    main()
