# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2019-10-18 01:54+0000\n"
"PO-Revision-Date: 2018-07-02 06:25+0000\n"
"Last-Translator: emmapeel <emma.peel@riseup.net>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 2.10.1\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Documentation\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<div class=\"tip\">\n"
msgstr "<div class=\"tip\">\n"

#. type: Plain text
#, no-wrap
msgid ""
"<p>If this section doesn't answer your questions, you can also look at our\n"
"[[FAQ|support/faq]].</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "</div>\n"
msgstr "</div>\n"

#. type: Plain text
msgid ""
"Read about how you can help [[improving Tails documentation|/contribute/how/"
"documentation]]."
msgstr ""

#. type: Plain text
msgid "- [[Introduction to this documentation|introduction]]"
msgstr ""

#. type: Title #
#, no-wrap
msgid "General information"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"doc/about.index\" raw=\"yes\" sort=\"age\"]]\n"
msgstr "[[!inline pages=\"doc/about.index.tr\" raw=\"yes\" sort=\"age\"]]\n"

#. type: Title #
#, no-wrap
msgid "Download, installation, and upgrade"
msgstr ""

#. type: Bullet: '  - '
msgid "[[Downloading without installing|install/download]]"
msgstr ""

#. type: Bullet: '  - '
msgid "[[Installing from another Tails (for PC)|install/win/clone-overview]]"
msgstr ""

#. type: Bullet: '  - '
msgid "[[Installing from another Tails (for Mac)|install/mac/clone-overview]]"
msgstr ""

#. type: Bullet: '  - '
msgid "[[Installing from Windows|install/win/usb-overview]]"
msgstr ""

#. type: Bullet: '  - '
msgid "[[Installing from macOS|install/mac/usb-overview]]"
msgstr ""

#. type: Bullet: '  - '
msgid "[[Installing from Linux (recommended)|install/linux/usb-overview]]"
msgstr ""

#. type: Bullet: '  - '
msgid ""
"[[Installing from Debian, Ubuntu, or Mint using the command line and GnuPG "
"(experts)|install/expert/usb-overview]]"
msgstr ""

#. type: Bullet: '  - '
msgid "[[Burning a DVD|install/dvd]]"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"  - [[!traillink Upgrading_a_Tails_USB_stick|upgrade]]\n"
"    - [[Manually_upgrading_from_your_Tails|upgrade/tails-overview]]\n"
"    - [[Manually_upgrading_from_Windows|upgrade/win-overview]]\n"
"    - [[Manually_upgrading_from_macOS|upgrade/mac-overview]]\n"
"    - [[Manually_upgrading_from_Linux|upgrade/linux-overview]]\n"
"    - [[Manually_upgrading_by_cloning_from_another_Tails|upgrade/clone-overview]]\n"
"    - [[!traillink Repairing_a_Tails_USB_stick_that_fails_to_start_after_an_upgrade|upgrade/repair]]\n"
"  - Uninstalling Tails or resetting a USB stick using\n"
"    [[!traillink Linux|reset/linux]],\n"
"    [[!traillink Windows|reset/windows]], or\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "    [[!traillink Mac|reset/mac]]\n"
msgstr ""

#. type: Title #
#, no-wrap
msgid "First steps with Tails"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"doc/first_steps.index\" raw=\"yes\" sort=\"age\"]]\n"
msgstr "[[!inline pages=\"doc/first_steps.index.tr\" raw=\"yes\" sort=\"age\"]]\n"

#. type: Title #
#, no-wrap
msgid "Connect to the Internet anonymously"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"doc/anonymous_internet.index\" raw=\"yes\" sort=\"age\"]]\n"
msgstr "[[!inline pages=\"doc/anonymous_internet.index.tr\" raw=\"yes\" sort=\"age\"]]\n"

#. type: Title #
#, no-wrap
msgid "Encryption and privacy"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"doc/encryption_and_privacy.index\" raw=\"yes\" sort=\"age\"]]\n"
msgstr "[[!inline pages=\"doc/encryption_and_privacy.index.tr\" raw=\"yes\" sort=\"age\"]]\n"

#. type: Title #
#, no-wrap
msgid "Work on sensitive documents"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"doc/sensitive_documents.index\" raw=\"yes\" sort=\"age\"]]\n"
msgstr "[[!inline pages=\"doc/sensitive_documents.index.tr\" raw=\"yes\" sort=\"age\"]]\n"

#. type: Title #
#, no-wrap
msgid "Advanced topics"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"doc/advanced_topics.index\" raw=\"yes\" sort=\"age\"]]\n"
msgstr "[[!inline pages=\"doc/advanced_topics.index.tr\" raw=\"yes\" sort=\"age\"]]\n"
