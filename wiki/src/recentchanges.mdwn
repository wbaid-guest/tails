[[!meta title="Recent changes"]]

See also the [recent activity on the task tracker](https://redmine.tails.boum.org/code/projects/tails/activity).

[[!inline pages="internal(recentchanges/change_*) and !title(change to forum/*) and !*/Discussion" 
template=recentchanges show=0 sort="age"]]
