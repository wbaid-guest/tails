# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2019-10-18 00:30+0000\n"
"PO-Revision-Date: 2018-08-24 14:26+0200\n"
"Last-Translator: Tails translators\n"
"Language-Team: \n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Poedit 1.8.11\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Upgrading a Tails USB stick\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!meta robots=\"noindex\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"doc/upgrade\" raw=\"yes\" sort=\"age\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<!-- This page only exists to provide the parent breadcrumbs in upgrade scenarios. -->\n"
msgstr ""
