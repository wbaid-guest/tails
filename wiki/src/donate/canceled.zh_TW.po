# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
msgid ""
msgstr ""
"Project-Id-Version: Tails l10n\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2017-06-07 17:03+0000\n"
"PO-Revision-Date: 2018-11-02 17:23+0000\n"
"Last-Translator: Weblate Admin <admin@example.com>\n"
"Language-Team: Tails Chinese translators <jxt@twngo.xyz>\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 2.19.1\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Payment canceled\"]]\n"
msgstr "[[!meta title=\"取消支付\"]]\n"

#. type: Plain text
msgid "Your payment was canceled."
msgstr "你的支付已被取消了。"

#. type: Plain text
msgid ""
"If you faced technical problems or were annoyed by something in the process, "
"we are very interested in hearing about it."
msgstr ""

#. type: Plain text
msgid "Please contact us at <mailto:tails-fundraising@boum.org>."
msgstr "請透過電郵 <mailto:tails-fundraising@boum.org>聯絡我們。"
