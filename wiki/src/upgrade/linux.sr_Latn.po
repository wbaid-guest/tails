# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2019-10-18 01:54+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: sr_Latn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Manually upgrade from Linux\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!meta robots=\"noindex\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!meta stylesheet=\"bootstrap.min\" rel=\"stylesheet\" title=\"\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!meta stylesheet=\"install/inc/stylesheets/assistant\" rel=\"stylesheet\" title=\"\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!meta stylesheet=\"install/inc/stylesheets/steps\" rel=\"stylesheet\" title=\"\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!meta stylesheet=\"install/inc/stylesheets/upgrade-linux\" rel=\"stylesheet\" title=\"\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<div class=\"hidden-step-1\"></div>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<div class=\"step-image\">[[!img install/inc/infography/os-linux.png link=\"no\" alt=\"\"]]</div>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<p class=\"start\">Start in Tails.</p>\n"
msgstr ""

#. type: Title =
#, no-wrap
msgid "Install an intermediary Tails\n"
msgstr ""

#. type: Plain text
msgid ""
"In this step, you will install an intermediary Tails using the Tails USB "
"image that you downloaded earlier."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"install/inc/steps/install_with_gnome_disks.inline\" raw=\"yes\" sort=\"age\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"install/inc/steps/restart_first_time.inline\" raw=\"yes\" sort=\"age\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"install/inc/steps/clone.inline\" raw=\"yes\" sort=\"age\"]]\n"
msgstr ""
