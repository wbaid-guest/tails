# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2018-03-11 11:54+0000\n"
"PO-Revision-Date: 2018-07-02 12:24+0000\n"
"Last-Translator: emmapeel <emma.peel@riseup.net>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 2.10.1\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Enable & use the persistent volume\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"[[!inline pages=\"doc/first_steps/persistence.caution\" raw=\"yes\" "
"sort=\"age\"]]\n"
msgstr ""
"[[!inline pages=\"doc/first_steps/persistence.caution.zh_TW\" raw=\"yes\" "
"sort=\"age\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!toc levels=1]]\n"
msgstr "[[!toc levels=1]]\n"

#. type: Title =
#, no-wrap
msgid "Enable the persistent volume\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"When starting Tails, in the\n"
"<span class=\"guilabel\">Encrypted Persistent Storage</span> section of\n"
"[[<span class=\"application\">Tails "
"Greeter</span>|startup_options#tails_greeter]], enter your passphrase and "
"click\n"
"<span class=\"button\">Unlock</span>.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!img startup_options/persistence.png link=\"no\" alt=\"\"]]\n"
msgstr "[[!img startup_options/persistence.png link=\"no\" alt=\"\"]]\n"

#. type: Title =
#, no-wrap
msgid "Use the persistent volume\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"To open the <span class=\"filename\">Persistent</span> folder and access "
"your\n"
"personal files and working documents, choose \n"
"<span class=\"menuchoice\">\n"
"  <span class=\"guimenu\">Places</span>&nbsp;▸\n"
"  <span class=\"guimenuitem\">Persistent</span></span>.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"For advanced users, to access the internal content of the persistent "
"volume\n"
"choose\n"
"<span class=\"menuchoice\">\n"
"  <span class=\"guimenu\">Places</span>&nbsp;▸\n"
"  <span class=\"guimenuitem\">Computer</span></span>, and open the folders\n"
"  <span class=\"filename\">live</span>&nbsp;▸\n"
"  <span class=\"filename\">persistence</span>&nbsp;▸\n"
"  <span class=\"filename\">TailsData_unlocked</span>.\n"
msgstr ""
