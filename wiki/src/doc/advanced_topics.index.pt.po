# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2019-09-24 16:20+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Bullet: '  - '
msgid ""
"[[!traillink Protection_against_cold_boot_attacks|advanced_topics/"
"cold_boot_attacks]]"
msgstr ""
"[[!traillink Proteção_contra_ataques_*cold_boot*|advanced_topics/"
"cold_boot_attacks]]"

#. type: Plain text
#, no-wrap
msgid ""
"  - [[!traillink Running_Tails_in_a_virtual_machine|advanced_topics/virtualization]]\n"
"    - [[!traillink <span_class=\"application\">VirtualBox</span>|advanced_topics/virtualization/virtualbox]]\n"
"    - [[!traillink <span_class=\"application\">GNOME_Boxes</span>|advanced_topics/virtualization/boxes]]\n"
"    - [[!traillink <span_class=\"application\">virt-manager</span>|advanced_topics/virtualization/virt-manager]]\n"
"  - Encrypted persistence\n"
"    - [[!traillink Change_the_passphrase_of_the_persistent_volume|advanced_topics/persistence/change_passphrase]]\n"
"    - [[!traillink Check_the_file_system_of_the_persistent_volume|advanced_topics/persistence/check_file_system]]\n"
"  - [[!traillink Accessing_resources_on_the_local_network|advanced_topics/lan]]\n"
"  - [[!traillink Enable_a_wireless_device|advanced_topics/wireless_devices]]\n"
msgstr ""

#, fuzzy
#~| msgid "[[!traillink Virtualization|advanced_topics/virtualization]]"
#~ msgid ""
#~ "[[!traillink Installing_additional_software|advanced_topics/"
#~ "additional_software]]"
#~ msgstr "[[!traillink Virtualização|advanced_topics/virtualization]]"

#, fuzzy
#~| msgid "[[!traillink Enable_MAC_Changer|advanced_topics/mac_changer]]"
#~ msgid ""
#~ "[[!traillink Enable_a_wireless_device|advanced_topics/wireless_devices]]"
#~ msgstr "[[!traillink Habilitar_MAC_Changer|advanced_topics/mac_changer]]"

#~ msgid "[[!traillink Enable_MAC_Changer|advanced_topics/mac_changer]]"
#~ msgstr "[[!traillink Habilitar_MAC_Changer|advanced_topics/mac_changer]]"
