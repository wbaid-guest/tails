[[!meta robots="noindex"]]

- Enter your passphrase to unlock your Persistent Storage. (from Greeter)

- Your Persistent Storage is unlocked. Restart Tails to lock it again. (from Greeter)

- Cannot delete the Persistent Storage on %s while in use. (from Persistence setup)

- Upgrade your Tails USB stick and keep your Persistent Storage. (from [[install/download]])

- This method might in rare occasions break the file system of your Persistent Storage. (from [[doc/first_steps/shutdown]])

- Create an apt-sources.list.d folder in your Persistent Storage. (from [[doc/first_steps/additional_software]])

- Freeing space in your Persistent Storage. (from [[doc/first_steps/additional_software]])

- Follow these steps to create a new password database and save it in your Persistent Storage for use in future working sessions. (from [[doc/encryption_and_privacy/manage_passwords]])

- When starting Tails, enable your Persistent Storage. (from [[doc/encryption_and_privacy/manage_passwords]])

- If you choose [Install Every Time], the package is saved in your Persistent Storage and will be reinstalled automatically every time you start Tails. (from [[doc/first_steps/additional_software]])

- Enable & use the Persistent Storage (from [[doc/first_steps/persistence]])

- Create the Persistent Storage (from [[install/win/usb]])

- You can create a Persistent Storage in the free space left on the USB stick. (from [[doc/first_steps/persistence]])

- Restart on your backup Tails and create the Persistent Storage on it. (from [[doc/first_steps/persistence/copy]])

- To create the Persistent Storage or change its configuration, choose Applications → Tails → Persistent Storage. (from [[doc/first_steps/persistence/configure]])

- An attacker in possession of your USB stick can know that there is a Persistent Storage on it. (from [[doc/first_steps/persistence/warnings]])

- If a Persistent Storage is detected on the USB stick, an additional section appears in Tails Greeter. (from [[doc/first_steps/startup_options]])

- Warnings about the Persistent Storage (from [[doc/first_steps/persistence]])

- To understand better how the Persistent Storage works, see our design document. (from [[support/faq]])

- The simplest way to carry around the documents that you want to use with Tails encrypted is to use the Persistent Storage. (from [[doc/encryption_and_privacy/encrypted_volumes]])

- To store your GnuPG keys and configuration across separate working sessions, you can activate the GnuPG feature of the Persistent Storage. (from [[doc/encryption_and_privacy/gpgapplet/decrypt_verify]])

- Save the database as keepassx.kdbx in the Persistent folder. (from [[doc/encryption_and_privacy/manage_passwords]])

- Read the documentation about the Persistent Storage to learn which settings can be made persistent across separate working sessions. (from [[doc/first_steps/introduction_to_gnome_and_the_tails_desktop]])
